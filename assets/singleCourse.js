console.log(window.location.search); //?courseId=6131a97ffbebc7b0d47833a4
let params = new URLSearchParams(window.location.search);

//method of URLSearchParams
	//URLSearchParams.get()
	let courseId =params.get("courseId");
	console.log(courseId)

let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice = document.querySelector('#coursePrice');
let enrollmentContainer = document.querySelector('#enrollmentContainer');



let token = localStorage.getItem('token')

fetch(`http://localhost:3000/api/courses/${courseId}`,
	{

		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then(result => {
	
	//console.log(result) - single course document

	courseName.innerHTML = result.name
	courseDesc.innerHTML = result.description
	coursePrice.innerHTML = result.price
	enrollmentContainer.innerHTML =
	`
		<button class="btn btn-primary btn-block">Enroll</button>
	`
})
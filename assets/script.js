//DOM manipulation

let navSession = document.querySelector('#navSession');
let navRegisterLink = document.querySelector('#register');
let userToken = localStorage.getItem("token");

//if user token not present
if (!userToken) {


	navSession.innerHTML =
	`
		<li class="nav-item">
			<a href="./login.html" class="nav-link">Login</a>
		</li>

	`
	navRegisterLink = 
	`
		<li class="nav-item">
			<a href="./register.html" class="nav-link">Register</a>
		</li>
	`
} else {

	navSession.innerHTML =
	`
		<li class="nav-item">
			<a href="./logout.html" class="nav-link">Sign out</a>
		</li>
	`
}
